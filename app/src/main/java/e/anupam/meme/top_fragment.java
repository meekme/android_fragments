package e.anupam.meme;

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;


public class top_fragment extends Fragment
{
    TopSectionListener activitycommander;
    public interface TopSectionListener
    {
        public void creatememe(String top,String bot);
    }

    EditText top;
    EditText bot;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState)
    {
        View view = inflater.inflate(R.layout.top_fragment, container,false);
        top = (EditText) view.findViewById(R.id.toptext);
        bot = (EditText) view.findViewById(R.id.bottext);
        final Button mybutton = (Button) view.findViewById(R.id.generate);
        mybutton.setOnClickListener(
                new View.OnClickListener(){
                    public void onClick(View v)
                    {
                        clicked(v);
                    }
                }
        );

        return view;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try{
            activitycommander = (TopSectionListener) activity; }
        catch (ClassCastException e)
        {
            throw new ClassCastException(activity.toString());
        }
    }

    public void clicked(View v)
    {
        activitycommander.creatememe(top.getText().toString(),bot.getText().toString());
    }

}
