package e.anupam.meme;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

public class bot_fragment extends android.support.v4.app.Fragment
{
    private static TextView toptext;
    private static TextView bottext;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.bot_fragment,container,false);
        toptext = (TextView) view.findViewById(R.id.textView);
        bottext = (TextView) view.findViewById(R.id.textView2);
        return view;
    }
    public void setmemetext(String top, String bot)
    {
        toptext.setText(top);
        bottext.setText(bot);
    }
}
