package e.anupam.meme;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class MainActivity extends AppCompatActivity implements top_fragment.TopSectionListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    @Override
    public void creatememe(String top, String bot)
    {
        bot_fragment lower;
        lower = (bot_fragment) getSupportFragmentManager().findFragmentById(R.id.fragment2);
        lower.setmemetext(top,bot);
    }
}
